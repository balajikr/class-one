import React, { useState } from "react";

import "./App.css";

// Stateless function component
export function StateLess() {
  /**
   * Define state values
   *
   * @return void
   *
   */
  const [login, changeState] = useState({ username: "", password: "" });

  /**
   * handle state value
   *
   *
   * @param {*} event
   * @return state
   *
   */

  const handleFieldsChange = e => {
    e.preventDefault();
    changeState({ ...login, [e.target.name]: e.target.value });
  };

  /**
   *
   * @param {*} event
   */

  const handleSubmit = e => {
    e.preventDefault();
    console.log("loginCredetials", login);
  };

  //return HTML JSX Element
  return (
    <div className="App">
      <form onSubmit={e => handleSubmit(e)}>
        <h1>Function component</h1>
        <label>Username: </label>
        <input
          type="text"
          name="username"
          value={login.username}
          onChange={e => handleFieldsChange(e)}
        />
        <br />
        <br />

        <label>Password: </label>
        <input
          type="password"
          name="password"
          value={login.password}
          onChange={e => handleFieldsChange(e)}
        />
        <br />
        <br />

        <button>Submit</button>
      </form>
    </div>
  );
}

//StateFull Component
export class StateFull extends React.Component {
  /**
   * Defining state
   */
  state = {
    fields: {
      username: "",
      password: ""
    }
  };

  /**
   *
   * @param {*} event
   *
   * @return state
   */
  handleFieldsChange = e => {
    e.preventDefault();
    const { fields } = this.state;
    fields[e.target.name] = e.target.value;
    this.setState({ fields });
  };

  /**
   *
   * @param {*} event
   *
   * @return state
   */

  handleSubmit = e => {
    e.preventDefault();
    console.log("loginCredentials", this.state.fields);
  };

  //render HTML JSX Element
  render() {
    const { fields } = this.state;
    return (
      <div className="App">
        <form onSubmit={e => this.handleSubmit(e)}>
          <h1>Class component</h1>
          <label>Username: </label>
          <input
            type="text"
            name="username"
            value={fields.username}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <label>Password: </label>
          <input
            type="password"
            name="password"
            value={fields.password}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <button>Submit</button>
        </form>
      </div>
    );
  }
}
