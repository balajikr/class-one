import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import * as serviceWorker from './serviceWorker';
import { StateLess, StateFull } from './App';


ReactDOM.render(<StateLess />, document.getElementById('app-root-hooks'));
ReactDOM.render(<StateFull/>, document.getElementById("app-root-without-hooks"))


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
